package hw_8;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.AbstractMap;
import java.util.List;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.*;

public class FamilyServiceTest {
    private FamilyService familyService;

    @BeforeEach
    void setUp() {
        FamilyDao familyDao = new CollectionFamilyDao();
        familyService = new FamilyService(familyDao);
    }

    @Test
    void testGetAllFamilies() {
        List<AbstractMap.SimpleEntry<Integer, Family>> allFamilies = familyService.getAllFamilies();
        assertNotNull(allFamilies);
        assertEquals(0, allFamilies.size());
    }

    @Test
    void testGetFamilyByIndex() {
        Human father = new Human("John", "Wick",1957);
        Human mother = new Human("Jane", "Wick",1957);
        familyService.createNewFamily(father, mother);

        Family family = familyService.getFamilyByIndex(0);
        assertNotNull(family);
        assertEquals(family, family);
    }

    @Test
    void testDeleteFamily() {
        Human father = new Human("John", "Wick",1957);
        Human mother = new Human("Jane", "Wick",1957);
        familyService.createNewFamily(father, mother);

        assertTrue(familyService.deleteFamily(0));
        assertNull(familyService.getFamilyByIndex(0));
    }

    @Test
    void testSaveFamily() {
        Human father = new Human("John", "Wick",1957);
        Human mother = new Human("Jane", "Wick",1957);
        Family testFamily = new Family(father, mother);

        familyService.saveFamily(testFamily);
        Family savedFamily = familyService.getFamilyByIndex(0);
        assertNotNull(savedFamily);
        assertEquals(testFamily, savedFamily);
    }

    @Test
    void testDisplayAllFamilies() {
        // Тест виводу на консоль, не потребує перевірки результату
        familyService.displayAllFamilies();
    }

    @Test
    void testGetFamiliesBiggerThan() {
        Human father = new Human("John", "Wick",1957);
        Human mother = new Human("Jane", "Wick",1957);
        familyService.createNewFamily(father, mother);
        Family family = familyService.getFamilyByIndex(0);
        Human child1 = new Human("Tom", "Wick",2020);
        family.addChild(child1);

        List<Family> families = familyService.getFamiliesBiggerThan(2);
        assertEquals(1, families.size());
    }

    @Test
    void testGetFamiliesLessThan() {
        Human father = new Human("John", "Wick",1957);
        Human mother = new Human("Jane", "Wick",1957);
        familyService.createNewFamily(father, mother);

        List<Family> families = familyService.getFamiliesLessThan(4);
        assertEquals(1, families.size());
    }

    @Test
    void testCountFamiliesWithMemberNumber() {
        Human father = new Human("John", "Wick",1957);
        Human mother = new Human("Jane", "Wick",1957);
        familyService.createNewFamily(father, mother);

        int count = familyService.countFamiliesWithMemberNumber(2);
        assertEquals(1, count);
    }

    @Test
    void testCreateNewFamily() {
        Human father = new Human("John", "Wick",1957);
        Human mother = new Human("Jane", "Wick",1957);
        familyService.createNewFamily(father, mother);

        Family family = familyService.getFamilyByIndex(0);
        assertNotNull(family);
        assertEquals(family, family);

    }

    @Test
    void testAdoptChild() {
        Human father = new Human("John", "Wick",1957);
        Human mother = new Human("Jane", "Wick",1957);
        familyService.createNewFamily(father, mother);

        Human child = new Human("Tom", "Wick",5);
        Family family = familyService.getFamilyByIndex(0);
        Family updatedFamily = familyService.adoptChild(family, child);
        assertEquals(1, updatedFamily.getChildren().size());
    }

    @Test
    void testDeleteAllChildrenOlderThen() {
        Human father = new Human("John", "Wick",1957);
        Human mother = new Human("Jane", "Wick",1957);
        familyService.createNewFamily(father, mother);

        Human child1 = new Human("Tom", "Wick",2020);
        Human child2 = new Human("Alice", "Wick",2010);
        Family family = familyService.getFamilyByIndex(0);
        family.addChild(child1);
        family.addChild(child2);
        familyService.saveFamily(family);

        familyService.deleteAllChildrenOlderThen(10);
        assertEquals(1, family.getChildren().size());
    }

    @Test
    void testCount() {
        Human father = new Human("John", "Wick", 1957);
        Human mother = new Human("Jane", "Wick",1957);
        familyService.createNewFamily(father, mother);

        assertEquals(1, familyService.count());
    }

    @Test
    void testGetPets() {
        Human father = new Human("John", "Wick",1957);
        Human mother = new Human("Jane", "Wick",1957);
        familyService.createNewFamily(father, mother);

        Set<Pet> pets = familyService.getPets(0);
        assertNotNull(pets);
    }

    @Test
    void testAddPet() {
        Human father = new Human("John", "Wick",1957);
        Human mother = new Human("Jane", "Wick",1957);
        familyService.createNewFamily(father, mother);

        Species dog = Species.DOG;
        Dog doggy = new Dog( "Tuzik");
        familyService.addPet(0, doggy);
        Set<Pet> pets = familyService.getPets(0);
        assertTrue(pets.contains(doggy));
    }
}
