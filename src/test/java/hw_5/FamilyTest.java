package hw_5;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    @Test
    void addChild() {
        Human mother = new Human("Alice", "Wick", 1980);
        Human father = new Human("Bob", "Wick", 1975);
        Pet pet = new Pet(Species.DOG, "Bullet");
        Family family = new Family(mother, father, pet);
        Human child = new Human("John", "Wick", 2005);

        Human[] originalChildren = family.getChildren();
        family.addChild(child);

        Human[] newChildren = family.getChildren();
        assertEquals(originalChildren.length + 1, newChildren.length);

        assertSame(child, newChildren[newChildren.length - 1]);
        assertSame(family, child.getFamily());
    }

    @Test
    void deleteChildIndex() {

            Human mother = new Human("Alice", "Wick", 1980);
            Human father = new Human("Bob", "Wick", 1975);
            Pet pet = new Pet(Species.DOG, "Bullet");
            Family family = new Family(mother, father, pet);
            Human child1 = new Human("John", "Wick", 2005);
            Human child2 = new Human("Emma", "Wick", 2010);
            family.addChild(child1);
            family.addChild(child2);

             boolean result = family.deleteChild(0);

            assertTrue(result);
            assertEquals(1, family.getChildren().length);
            assertNotSame(child1, family.getChildren()[0]);
        }

    @Test
    void deleteChildBadIndex() {

            Human mother = new Human("Alice", "Wick", 1980);
            Human father = new Human("Bob", "Wick", 1975);
            Pet pet = new Pet(Species.DOG, "Bullet");
            Family family = new Family(mother, father, pet);
            Human child1 = new Human("John", "Wick", 2005);
            family.addChild(child1);

            boolean result = family.deleteChild(1);

            assertFalse(result);
            assertEquals(1, family.getChildren().length);
        }

    @Test
    void countFamily() {
        int familySize = 2;

        if (familySize > 2){
            Human mother = new Human("Alice", "Wick", 1980);
            Human father = new Human("Bob", "Wick", 1975);
            Human child1 = new Human("John", "Wick", 2005);
            Human child2 = new Human("Emma", "Wick", 2010);
            Pet pet = new Pet(Species.DOG, "Bullet");
            Family family = new Family(mother, father, pet);
            family.addChild(child1);
            family.addChild(child2);

             familySize = family.countFamily();

            assertEquals(4, familySize);
        } else {
            Human mother = new Human("Alice", "Wick", 1980);
            Human father = new Human("Bob", "Wick", 1975);
            Pet pet = new Pet(Species.DOG, "Bullet");
            Family family = new Family(mother, father, pet);

            assertEquals(2, familySize);
        }
    }

    @Test
    void testToString() {

        Human mother = new Human("Alice", "Wick", 1980);
        Human father = new Human("Bob", "Wick", 1975);
        Pet pet = new Pet(Species.DOG, "Bullet");
        Family family = new Family(mother, father, pet);

        String expectedString = "Family {\n" +
                "  Mother: Alice\n" +
                "  Father: Bob\n" +
                "  Children: []\n" +
                "  Pet: DOG { Bullet, 0, 0, null }\n" +
                "}";

        String familyString = family.toString();

        assertEquals(expectedString, familyString);
    }
}