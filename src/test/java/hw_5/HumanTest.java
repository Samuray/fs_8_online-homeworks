package hw_5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void testToString() {

        String name = "John";
        String surname = "Wick";
        int year = 1980;
        int iq = 120;
        String[][] schedule = {{"Monday", "Do chores"}, {"Tuesday", "Workout"}};


        Human human = new Human(name, surname, year, iq, null, schedule);
        String expectedString = "Human {\n" + "John Wick, 1980, 120, [[Monday, Do chores], [Tuesday, Workout]], \n" + "}";

        String humanString = human.toString();

        assertEquals(expectedString, humanString);

    }
}