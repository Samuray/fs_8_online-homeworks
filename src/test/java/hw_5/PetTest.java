package hw_5;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PetTest {

    @Test
    void testToString() {
        Species species = Species.DOG;
        String nickname = "Bullet";
        int age = 3;
        int trickLevel = 5;
        String[] habits = {"play", "friendly"};

        Pet pet = new Pet(species, nickname, age, trickLevel, habits);
        String expectedString = "DOG { Bullet, 3, 5, [play, friendly] }";

        String petString = pet.toString();

        assertEquals(expectedString, petString);

    }

}
