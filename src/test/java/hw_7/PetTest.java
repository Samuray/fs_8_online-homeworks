package hw_7;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class PetTest {

    @Test
    void testToString() {
        Species species = Species.DOG;
        String nickname = "Bullet";
        int age = 3;
        int trickLevel = 5;
        String[] habitsArray = {"play", "friendly"};

        Pet pet = new Dog( nickname, age, trickLevel, habitsArray);
        String expectedString = "DOG { Bullet, 3, 5, [play, friendly] }";

        String petString = pet.toString();

        assertEquals(expectedString, petString);

    }

}
