package hw_7;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.HashMap;
import java.util.Map;

public class HumanTest {
    @Test
    void testToString() {
        String name = "John";
        String surname = "Wick";
        int year = 1980;
        int iq = 120;

        Map<String, String> schedule = new HashMap<>();
        schedule.put("Monday", "Do chores");
        schedule.put("Tuesday", "Workout");

        Human human = new Human(name, surname, year, iq, null, schedule);
        String expectedString = "Human {\n" +
                "John Wick, 1980, 120, {Monday=Do chores, Tuesday=Workout}, \n" +
                "}";

        String humanString = human.toString();

        assertEquals(expectedString, humanString);
    }
}
