package hw_7;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

class FamilyTest {

    @Test
    void addChild() {
        Human mother = new Human("Alice", "Wick", 1980);
        Human father = new Human("Bob", "Wick", 1975);
        Pet pet = new Dog( "Bullet");
        Family family = new Family(mother, father, pet);
        Human child = new Human("John", "Wick", 2005);

        ArrayList<Human> originalChildren = new ArrayList<>(family.getChildren());
        family.addChild(child);

        ArrayList<Human> newChildren = new ArrayList<>(family.getChildren());
        assertEquals(originalChildren.size() + 1, newChildren.size());

        assertTrue(newChildren.contains(child));
        assertEquals(family, child.getFamily());
    }

    @Test
    void deleteChildIndex() {
        Human mother = new Human("Alice", "Wick", 1980);
        Human father = new Human("Bob", "Wick", 1975);
        Pet pet = new Dog( "Bullet");
        Family family = new Family(mother, father, pet);
        Human child1 = new Human("John", "Wick", 2005);
        Human child2 = new Human("Emma", "Wick", 2010);
        family.addChild(child1);
        family.addChild(child2);

        boolean result = family.deleteChild(0);

        assertTrue(result);
        assertEquals(1, family.getChildren().size());
        assertFalse(family.getChildren().contains(child1));
    }

    @Test
    void deleteChildBadIndex() {
        Human mother = new Human("Alice", "Wick", 1980);
        Human father = new Human("Bob", "Wick", 1975);
        Pet pet = new Dog( "Bullet");
        Family family = new Family(mother, father, pet);
        Human child1 = new Human("John", "Wick", 2005);
        family.addChild(child1);

        boolean result = family.deleteChild(1);

        assertFalse(result);
        assertEquals(1, family.getChildren().size());
    }

    @Test
    void countFamily() {
        int familySize = 2;

        if (familySize > 2) {
            Human mother = new Human("Alice", "Wick", 1980);
            Human father = new Human("Bob", "Wick", 1975);
            Human child1 = new Human("John", "Wick", 2005);
            Human child2 = new Human("Emma", "Wick", 2010);
            Pet pet = new Dog( "Bullet");
            Family family = new Family(mother, father, pet);
            family.addChild(child1);
            family.addChild(child2);

            familySize = family.countFamily();

            assertEquals(4, familySize);
        } else {
            Human mother = new Human("Alice", "Wick", 1980);
            Human father = new Human("Bob", "Wick", 1975);
            Pet pet = new Dog( "Bullet");
            Family family = new Family(mother, father, pet);

            assertEquals(2, familySize);
        }
    }

    @Test
    void testToString() {
        Human mother = new Human("Alice", "Wick", 1980);
        Human father = new Human("Bob", "Wick", 1975);
        Pet pet = new Dog( "Bullet");
        Family family = new Family(mother, father, pet);

        Set<Pet> pets = new HashSet<>();
        pets.add(pet);

        String expectedString = "Family {\n" +
                "  Mother: Alice\n" +
                "  Father: Bob\n" +
                "  Children: " + new ArrayList<>(family.getChildren()) + "\n" +
                "  Pet: " + pets.toString() + "\n" +
                "}";

        String familyString = family.toString();

        assertEquals(expectedString, familyString);
    }
}