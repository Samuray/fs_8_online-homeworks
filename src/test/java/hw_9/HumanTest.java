package hw_9;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

public class HumanTest {
    @Test
    public void testFormatting() {
        Human person = new Human("Mary", "Karleone", Period.between(LocalDate.parse("27/02/1987", DateTimeFormatter.ofPattern("dd/MM/yyyy")), LocalDate.now()).toTotalMonths() * 30L * 24 * 60 * 60 * 1000); // Пример даты рождения
        String expected = "36 years, 6 months и 19 days";
        String actual = person.describeAge();
        assertEquals(expected, actual);
    }
}
