package hw_6;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ManTest {
    @Test
    void repairCar() {
        Man man = new Man();

        String validCar = "Toyota Camry";
        assertEquals(validCar, man.repairCar(validCar));

        String nullCar = null;
        String expectedNullResult = "Nothing to fix!";
        assertEquals(expectedNullResult, man.repairCar(nullCar));

        String emptyCar = "";
        String expectedEmptyResult = "Nothing to fix!";
        assertEquals(expectedEmptyResult, man.repairCar(emptyCar));
    }

}
