package hw_6;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class WomanTest {
    @Test
    void makeUp() {
            Woman woman = new Woman();
            boolean wearsMakeup = true;
            String expected = "Make up!";
            assertEquals(expected, woman.makeup(wearsMakeup));
        }

        @Test
        public void notMakeUp() {
            Woman woman = new Woman();
            boolean wearsMakeup = false;
            String expected = "Don't wear makeup!";
            assertEquals(expected, woman.makeup(wearsMakeup));
        }

    }

