package hw_7;

public class Fish extends Pet {
    @Override
    void respond() {}

    public Fish() {
        super();
    }

    public Fish(String name) {
        super(name);
        setSpecies(Species.FISH);
    }

    public Fish(String name, int num, int trick, String... habits) {
        super(name, num, trick, habits);
        setSpecies(Species.FISH);
    }
}
