package hw_7;

public enum Species {
    CAT, DOG, HAMSTER, PARROT, FISH, ROBOCAT, DOMESTICCAT, UNKNOWN;
}
