package hw_7;

import java.util.HashSet;
import java.util.Set;

public abstract class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(String... values) {
        habits = new HashSet<>();
        for (String value : values) {
            this.habits.add(value);
        }
    }


    public void eat() {
        System.out.println("I'm eating!");
    }
    abstract void respond();

    @Override
    public boolean equals(Object obj) {
        if(obj == this) return true;
        if(obj == null) return false;
        if(!(obj instanceof Pet that)) return false;
        return this.species.equals(that.species)
                &&this.nickname.equals(that.nickname)
                && this.age == that.age
                && this.trickLevel == that.trickLevel
                && this.habits.size() == that.habits.size();
    }

    @Override
    public String toString() {
        String infoPet = this.species +  " { " + this.nickname + ", " +  this.age + ", " +  this.trickLevel + ", " + this.habits + " }";
        return infoPet;
    }

    @Override
    protected void finalize() {
        System.out.println("Питомец уходит в небытие...");
    }

    public Pet() {
        this.species = Species.UNKNOWN;
    }
    public Pet(String name) {
        this.nickname = name;
    }
    public Pet(String name, int num, int trick,String... habits) {
        this.nickname = name;
        this.age = num;
        this.trickLevel = trick;
        setHabits(habits);
    }

}
