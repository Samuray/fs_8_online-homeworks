package hw_8;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        DomesticCat myCat = new DomesticCat( "Murzik", 5, 60, "eat", "drink", "sleep", "meows" );
        System.out.println(myCat.toString());
        Dog myDog = new Dog( "Sharik", 3, 50,  "jump","gives paw");
        System.out.println(myDog.toString());
        RoboCat robo = new RoboCat( "Zhorik", 2, 90, "eat", "floats", "walks on its hind legs");
        System.out.println(robo.toString());
        myDog.eat();
        robo.respond();
        myCat.foul();
        Fish myFish = new Fish("Jaw",2, 99, "eat");

        Dog newDog = new Dog();
        System.out.println(newDog.toString());

        System.out.println("=============" + "\n" + myCat.equals(myDog) + "\n" + "=============");

        DayOfWeek monday = DayOfWeek.MONDAY;
        DayOfWeek friday = DayOfWeek.FRIDAY;

        Human mother = new Human("Jane", "Karleone",1950);
        Human father = new Human("Vito", "Karleone", 1948);
        Human sun = new Human("Bob", "Karleone", 1985);
        System.out.println(sun.toString());
        Human daughter = new Human("Mary", "Karleone", 1987);

        Map<String, String> map = new HashMap<>();
        map.put(monday.name(), "something");
        map.put(friday.name(), "something2");

        Human man = new Human("Michael", "Karleone", 1977, 90, new Family(mother, father, myDog, robo), map);
        System.out.println(man.toString());
        man.greetPet();
        man.describePet();

        Man chel = new Man("Vasya", "Vasilek", 1990);
        chel.greetPet();
        System.out.println(chel.repairCar("BMW"));
        Woman chel2 = new Woman("Motya", "Tyotya", 1990);
        chel2.greetPet();
        System.out.println(chel2.makeup(false));

        Family myFamily = new Family(mother, father, robo, myCat);
        myFamily.addChild(sun);
        myFamily.addChild(daughter);
        int familyCount = myFamily.countFamily();
        System.out.println("=========================" + "\n" + "В этой семье: " +  familyCount + " человека!" + "\n" + "=========================");
        System.out.println(myFamily.toString());
        myFamily.deleteChild(1);
        int newFamilyCount = myFamily.countFamily();
        System.out.println("=========================" + "\n" + "В этой семье: " +  newFamilyCount + " человека!" + "\n" + "=========================");
        System.out.println(myFamily.toString());



        Family myFamily_2 = new Family(mother,father,myDog, robo);
        myFamily_2.addChild(daughter);
        int familyCount_2 = myFamily_2.countFamily();
        System.out.println("=========================" + "\n" + "В этой семье: " +  familyCount_2 + " человека!" + "\n" + "=========================");
        System.out.println(myFamily_2.toString());


////       Раскоментировать для проверки загруженности памяти
//        for(int i = 0; i < 250000; i++) {
//            Human human = new Human();
//            human = null;
//        }


        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        System.out.println("\nCreating a new family...");
        familyController.createNewFamily(new Human("JOHN","jhonson", 30), new Human("JANE","janson", 28));
        familyController.createNewFamily(new Human("BOB","Bobson", 30), new Human("BABY","Bobson", 28));

        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");


        System.out.println("\nCount of families: " + familyController.count());

        System.out.println("\nDeleting family at index " + 0 + "...");
        familyController.deleteFamily(0);

        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        familyController.createNewFamily(chel, chel2);

        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        familyController.saveFamily(myFamily_2);
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        System.out.println("\nDeleting family at object...");
        familyController.deleteFamily(myFamily_2);
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        familyController.saveFamily(myFamily_2);
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        int num = 3;
        System.out.println("Families with more " + num +  " people");
        System.out.println(familyController.countFamiliesWithMemberNumber(num));
        System.out.println("================================================================================================");

        int col = 2;
        System.out.println("Families Bigger " + col +  " people");
        System.out.println(familyController.getFamiliesBiggerThan(col));
        System.out.println("================================================================================================");
        col = 3;
        System.out.println("Families Less " + num +  " people");
        System.out.println(familyController.getFamiliesLessThan(col));
        System.out.println("================================================================================================");

        System.out.println(familyController.adoptChild(myFamily_2, sun));
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        familyController.deleteAllChildrenOlderThen(35);
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        System.out.println(familyController.getPets(2));
        System.out.println("================================================================================================");

        familyController.addPet(1, myCat, myFish);
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        System.out.println(familyController.getAllFamilies());
        System.out.println("================================================================================================");
        System.out.println(familyController.getFamilyByIndex(0));
        System.out.println("================================================================================================");
    }
}
