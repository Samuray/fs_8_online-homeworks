package hw_8;

public enum Species {
    CAT, DOG, HAMSTER, PARROT, FISH, ROBOCAT, DOMESTICCAT, UNKNOWN;
}
