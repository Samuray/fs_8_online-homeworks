package hw_8;

import java.util.AbstractMap;
import java.util.List;
import java.util.Set;

public interface FamilyDao {
    List<AbstractMap.SimpleEntry<Integer, Family>> getAllFamilies();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);

     void displayAllFamilies();

    List<Family> getFamiliesBiggerThan(int num);

    List<Family> getFamiliesLessThan(int num);

    int countFamiliesWithMemberNumber(int num);

    void createNewFamily(Human father, Human mother);

    Family adoptChild(Family family, Human child);

    void deleteAllChildrenOlderThen(int age);

    int count();

    Set<Pet> getPets(int index);

    void addPet(int index, Pet... pet);
}
