package hw_3;

import java.util.Arrays;
import java.util.Scanner;

public class TaskPlanner {
    public static String getDay(String[][] arr, String day) {
        String yourTasks = "";

                switch (day) {
                    case "Sunday":
                        yourTasks = "Your tasks for " + arr[0][0] + ": " + arr[0][1];
                        break;
                    case "Monday":
                        yourTasks = "Your tasks for " + arr[1][0] + ": " + arr[1][1];
                        break;
                    case "Tuesday":
                        yourTasks = "Your tasks for " + arr[2][0] + ": " + arr[2][1];
                        break;
                    case "Wednesday":
                        yourTasks = "Your tasks for " + arr[3][0] + ": " + arr[3][1];
                        break;
                    case "Thursday":
                        yourTasks = "Your tasks for " + arr[4][0] + ": " + arr[4][1];
                        break;
                    case "Friday":
                        yourTasks = "Your tasks for " + arr[5][0] + ": " + arr[5][1];
                        break;
                    case "Saturday":
                        yourTasks = "Your tasks for " + arr[6][0] + ": " + arr[6][1];
                        break;
                    default:
                        System.out.println("Sorry, I don't understand you, please enter the day of the week.");
                        break;
                }
        return yourTasks;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[][] scedule = new String[7][2];
        boolean isTrue = true;

        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work.";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film.";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to gym.";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "doing something.";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "go to shop; watch a tv.";
        scedule[5][0] = "Friday";
        scedule[5][1] = "go to bar.";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "sleeping all day.";
        System.out.println(Arrays.deepToString(scedule));

        while (isTrue) {
            System.out.println("Please, input the day of the week: ");
            String day = scan.nextLine().trim().toLowerCase();
            day = Character.toUpperCase(day.charAt(0)) + day.substring(1);

            if(day.equals("Exit")) {
                isTrue = false;
                System.out.println("Program closed!");
            } else {
                System.out.println(getDay(scedule, day));
            }

        }
    }
}
