package hw_4;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }


    public void eat() {
        System.out.println("I'm eating!");
    }
    public void respond() {
        System.out.println("Hello, host. I am " + nickname + ". I missed!");
    }
    public void foul() {
        System.out.println("You need to cover your tracks well...");
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this) return true;
        if(obj == null) return false;
        if(!(obj instanceof Pet that)) return false;
        return this.species.equals(that.species)
                && this.nickname.equals(that.nickname)
                && this.age == that.age
                && this.trickLevel == that.trickLevel
                && Arrays.equals(this.habits, that.habits);
    }

    @Override
    public String toString() {
        String infoPet = species + " { " + nickname + ", " +  age + ", " +  trickLevel + ", " + Arrays.toString(habits) + " }";
        return infoPet;
    }

    public Pet() {}
    public Pet(String spec, String name) {
        this.species = spec;
        this.nickname = name;
    }
    public Pet(String spec, String name, int num, int trick, String[] hab) {
        this.species = spec;
        this.nickname = name;
        this.age = num;
        this.trickLevel = trick;
        this.habits = hab;
    }

}
