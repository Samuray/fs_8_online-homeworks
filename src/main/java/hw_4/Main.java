package hw_4;

public class Main {
    public static void main(String[] args) {
        Pet myCat = new Pet("cat", "Murzik", 5, 60, new String[]{"eat", "drink", "sleep"});
        System.out.println(myCat.toString());
        Pet myDog = new Pet("dog", "Sharik", 3, 50, new String[]{"eat", "jump","gives paw"});
        System.out.println(myDog.toString());
        myCat.eat();
        myCat.respond();
        myCat.foul();

        System.out.println("=============" + "\n" + myCat.equals(myDog) + "\n" + "=============");


        Human mother = new Human("Jane", "Karleone",1950);
        Human father = new Human("Vito", "Karleone", 1948);
        Human sun = new Human("Bob", "Karleone", 1985);
        System.out.println(sun.toString());
        Human daughter = new Human("Mary", "Karleone", 1987);

        Human man = new Human("Michael", "Karleone", 1977,90, new Family(mother, father, myDog), new String[][]{
                {"data", "something"},
                {"data2", "something2"}
        } );
        System.out.println(man.toString());
        man.greetPet();
        man.describePet();

        Family myFamily = new Family(mother, father, myCat);
        myFamily.addChild(sun);
        myFamily.addChild(daughter);
        int familyCount = myFamily.countFamily();
        System.out.println("=========================" + "\n" + "В этой семье: " +  familyCount + " человека!" + "\n" + "=========================");
        System.out.println(myFamily.toString());
        myFamily.deleteChild(0);
        int newFamilyCount = myFamily.countFamily();
        System.out.println("=========================" + "\n" + "В этой семье: " +  newFamilyCount + " человека!" + "\n" + "=========================");
        System.out.println(myFamily.toString());



        Family myFamily_2 = new Family(mother,father,myDog);
        myFamily_2.addChild(sun);
        int familyCount_2 = myFamily_2.countFamily();
        System.out.println("=========================" + "\n" + "В этой семье: " +  familyCount_2 + " человека!" + "\n" + "=========================");
        System.out.println(myFamily_2.toString());

    }

}
