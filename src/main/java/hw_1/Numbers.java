package hw_1;

import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        System.out.println("Let the game begin!");

        Random rand = new Random();
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your name: ");
        String name = scan.nextLine();

        while (true) {
            int num = rand.nextInt(101);
            System.out.println(num);

            System.out.println("Enter your number: ");
            int userNum = scan.nextInt();

            switch (Integer.compare(userNum, num)) {
                case -1:
                    System.out.println("Your number is too small. Please, try again.");
                    break;
                case 0:
                    System.out.println("Congratulations, " + name + "!");
                    break;
                case 1:
                    System.out.println("Your number is too big. Please, try again.");
                    break;
                default: {
                    System.out.println("Game over!");
                    break;
                }

            }
        }
    }
}
