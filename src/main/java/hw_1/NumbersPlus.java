package hw_1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class NumbersPlus {
    public static int[] postNewArr(int[] array, int num){

        int newSize = array.length + 1;
        int[] newArray = new int[newSize];

        System.arraycopy(array, 0, newArray, 0, array.length);

        newArray[newSize - 1] = num;

        return newArray;
    }

    public static void main(String[] args) {
        System.out.println("Let the game begin!");

        Random rand = new Random();
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your name: ");
        String name = scan.nextLine();

        int[] arr = new int[0];

        while (true) {
            int num = rand.nextInt(101);
            System.out.println(num);

            System.out.println("Please enter your number: ");
            while (!scan.hasNextInt()) {
                System.out.println("You did not enter a number, please enter your number: ");
                scan.next();
            }
            int userNum = scan.nextInt();

            arr = postNewArr(arr, userNum);

            switch (Integer.compare(userNum, num)) {
                case -1:
                    System.out.println("Your number is too small. Please, try again.");
                    break;
                case 0:
                    System.out.println("Congratulations," + name + "!");
                    System.out.println("Your numbers: " + Arrays.toString(arr));
                    break;
                case 1:
                    System.out.println("Your number is too big. Please, try again.");
                    break;
                default:
                    System.out.println("Game over!");
            }
        }
    }


}
