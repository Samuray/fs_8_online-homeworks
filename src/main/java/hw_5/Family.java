package hw_5;


import java.util.Arrays;

public class Family {
   private Human mother;
   private Human father;
   private Human[] children;
   private Pet myPet;

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getMyPet() {
        return myPet;
    }

    public void setMyPet(Pet myPet) {
        this.myPet = myPet;
    }

    public Human[] addChild(Human child) {
        int newSize = children.length + 1;
        Human[] newChild = new Human[newSize];

        System.arraycopy(children, 0, newChild, 0, children.length);

        newChild[newSize - 1] = child;
        child.setFamily(this);
        return children = newChild;
    }

    public boolean deleteChild(int num) {
        if (num < 0 || num >= children.length) {
            System.out.println("Некорректный индекс");
            return false;
        }

        Human[] newArray = new Human[children.length - 1];
        for (int i = 0, k = 0; i < children.length; i++) {
            if (i == num) {
                continue;
            }
            newArray[k++] = children[i];
            children = newArray;
        }
        return true;
    }


    public int countFamily() {
        int count = 2;
        for (int i = 0; i < children.length; i++) {
            count++;
        }
        return count;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Family {\n");
        sb.append("  Mother: ").append(mother.getName()).append("\n");
        sb.append("  Father: ").append(father.getName()).append("\n");
        sb.append("  Children: ").append(Arrays.toString(children)).append("\n");
        sb.append("  Pet: ").append(myPet.toString()).append("\n");
        sb.append("}");
        return sb.toString();
    }

    @Override
    protected void finalize() {
        System.out.println("Семья уходит в небытие...");
    }

    public Family(Human mam, Human dad, Pet myPet) {
        if (mam != null && dad != null) {
            this.mother = mam;
            this.mother.setFamily(this);
            this.father = dad;
            this.father.setFamily(this);
            this.myPet = myPet;
            this.children = new Human[0];
        } else {
            System.out.println("Error: Both mother and father must be present to create a family.");
        }
    }
}
