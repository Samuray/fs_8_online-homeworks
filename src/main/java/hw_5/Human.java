package hw_5;


import java.util.Arrays;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String[][] schedule;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }
    public int getIq() {
        return iq;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public String[][] getSchedule() {
        return schedule;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }


    public void greetPet() {
            System.out.println("Hello, " + family.getMyPet().getNickname() + "!");
    }
    public void describePet() {
            String trick;
            if( family.getMyPet().getTrickLevel() > 50) {
                trick = "very cunning!";
                System.out.println("I have " + family.getMyPet().getSpecies() + ", it is " + family.getMyPet().getAge() + " years old, it is " + trick);
            } else {
                trick = "almost not cunning!";
                System.out.println("I have " + family.getMyPet().getSpecies() + ", it is " + family.getMyPet().getAge() + " years old, it is " + trick);
            }

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Human {\n");
        sb.append(getName()).append(" ");
        sb.append(getSurname()).append(", ");
        sb.append(getYear()).append(", ");
        if(iq != 0) {
            sb.append(getIq()).append(", ");
        }
        if(schedule != null && schedule.length > 0) {
            sb.append(Arrays.deepToString(schedule)).append(", ");
        }
        sb.append("\n").append("}");
        return sb.toString();
    }

    @Override
    protected void finalize() {
        System.out.println("Человек уходит в небытие...");
    }

    public Human() {}
    public Human(String firstName, String surName, int num) {
        this.name = firstName;
        this.surname = surName;
        this.year = num;
    }
    public Human(String firstName, String surName, int num, Family family) {
        this.name = firstName;
        this.surname = surName;
        this.year = num;
        this.family = family;
    }
    public Human(String firstName, String surName, int num, int humanIq, Family myFamily, String[][] sched ) {
        this.name = firstName;
        this.surname = surName;
        this.year = num;
        this.iq = humanIq;
        this.family = myFamily;
        this.schedule = sched;
    }
}
