package hw_9;

public enum Species {
    CAT, DOG, HAMSTER, PARROT, FISH, ROBOCAT, DOMESTICCAT, UNKNOWN;
}
