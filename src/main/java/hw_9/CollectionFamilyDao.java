package hw_9;

import java.time.Year;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CollectionFamilyDao implements FamilyDao {
    private final List<Family> families;

    public CollectionFamilyDao() {
        this.families = new ArrayList<>();
    }

    @Override
    public List<AbstractMap.SimpleEntry<Integer, Family>> getAllFamilies() {
        return IntStream.range(0, families.size())
                .mapToObj(index -> new AbstractMap.SimpleEntry<>(index, families.get(index)))
                .collect(Collectors.toList());
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < families.size()) {
            return families.get(index);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < families.size()) {
            families.remove(index);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return families.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        if (families.contains(family)) {
            families.remove(family);
        }
        families.add(family);
    }

    @Override
    public void displayAllFamilies() {
        for (Family family : families) {
            System.out.println(family);
        }
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int num) {
        List<Family> result = new ArrayList<>();
        for (Family family : families) {
            if (family.countFamily() > num) {
                result.add(family);
            }
        }
        return result;
    }

    @Override
    public List<Family> getFamiliesLessThan(int num) {
        List<Family> result = new ArrayList<>();
        for (Family family : families) {
            if (family.countFamily() < num) {
                result.add(family);
            }
        }
        return result;
    }

    @Override
    public int countFamiliesWithMemberNumber(int num) {
        int count = 0;
        for (Family family : families) {
            if (family.countFamily() == num) {
                count++;
            }
        }
        return count;
    }

    @Override
    public void createNewFamily(Human father, Human mother) {
        Family newFamily = new Family(father, mother);
        saveFamily(newFamily);
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        saveFamily(family);
        return family;
    }

    @Override
    public void deleteAllChildrenOlderThen(int age) {
        int currentYear = Year.now().getValue();

        for (Family family : families) {
            List<Human> childrenToRemove = new ArrayList<>();
            for (Human child : family.getChildren()) {
                if ((currentYear - child.getBirthDate()) > age) {
                    childrenToRemove.add(child);
                }
            }
            family.getChildren().removeAll(childrenToRemove);
        }
    }

    @Override
    public int count() {
        return families.size();
    }

    @Override
    public Set<Pet> getPets(int index) {
        Set<Pet> newPet = new HashSet<>();
        if (index >= 0 && index < families.size()) {
            newPet =  families.get(index).getMyPet();
        }
        return newPet;
    }

    @Override
    public void addPet(int index, Pet... pet) {
        if (index >= 0 && index < families.size()) {
            families.get(index).setMyPet(pet);
        }
    }
}

