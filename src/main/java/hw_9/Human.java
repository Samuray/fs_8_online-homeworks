package hw_9;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Iterator;
import java.util.Map;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;
    private Map<String, String> schedule;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public long getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }
    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }
    public int getIq() {
        return iq;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public Map<String, String> getSchedule() {
        return schedule;
    }
    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }


    public void greetPet() {
        Iterator <Pet> iterator = family.getMyPet().iterator();
        while (iterator.hasNext())
            System.out.println("Hello, " + iterator.next().getNickname() + "!");
    }
    public void describePet() {
        Iterator <Pet> iterator = family.getMyPet().iterator();
        while (iterator.hasNext()) {
            String trick;
            Pet currentPet = iterator.next();
            if( currentPet .getTrickLevel() > 50) {
                trick = "very cunning!";
                System.out.println("I have " + currentPet .getSpecies() + ", it is " + currentPet .getAge() + " years old, it is " + trick);
            } else {
                trick = "almost not cunning!";
                System.out.println("I have " + currentPet .getSpecies() + ", it is " + currentPet .getAge() + " years old, it is " + trick);
            }
        }
    }

    public String describeAge() {
        LocalDateTime birthDate = LocalDateTime.now().minusNanos(this.birthDate * 1_000_000);
        LocalDate currentDate = LocalDate.now();
        Period period = Period.between(birthDate.toLocalDate(), currentDate);

       return  period.getYears() + " years, " + period.getMonths() + " months и " + period.getDays() + " days";
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Human {\n");
        sb.append(getName()).append(" ");
        sb.append(getSurname()).append(", ");
        sb.append(describeAge()).append(", ");
        if(iq != 0) {
            sb.append(getIq()).append(", ");
        }
        if(schedule != null && !schedule.isEmpty()) {
            sb.append(this.schedule).append(", ");
        }
        sb.append("\n").append("}");
        return sb.toString();
    }

    @Override
    protected void finalize() {
        System.out.println("Человек уходит в небытие...");
    }

    public Human() {}
    public Human(String firstName, String surName, long num) {
        this.name = firstName;
        this.surname = surName;
        this.birthDate = num;
    }
    public Human(String firstName, String surName, long num, int humanIq) {
        this.name = firstName;
        this.surname = surName;
        this.birthDate = num;
        this.iq = humanIq;
    }
    public Human(String firstName, String surName, long num, Family family) {
        this.name = firstName;
        this.surname = surName;
        this.birthDate = num;
        this.family = family;
    }
    public Human(String firstName, String surName, long num, int humanIq, Family myFamily, Map<String, String> sched ) {
        this.name = firstName;
        this.surname = surName;
        this.birthDate = num;
        this.iq = humanIq;
        this.family = myFamily;
        this.schedule = sched;
    }
}
