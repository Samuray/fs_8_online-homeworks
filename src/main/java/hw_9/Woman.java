package hw_9;

import java.util.Map;

public final class Woman extends Human {

    public String makeup(boolean a) {
        return a ? "Make up!": "Don't wear makeup!";
    }
    @Override
    public void greetPet() {
        System.out.println("Hello, I'm a woman!");
    }

    public Woman() {
    }

    public Woman(String firstName, String surName, long num) {
        super(firstName, surName, num);
    }

    public Woman(String firstName, String surName, long num, Family family) {
        super(firstName, surName, num, family);
    }

    public Woman(String firstName, String surName, long num, int humanIq, Family myFamily, Map<String, String> sched) {
        super(firstName, surName, num, humanIq, myFamily, sched);
    }
}
