package hw_9;

import java.util.AbstractMap;
import java.util.List;
import java.util.Set;

public class FamilyService implements FamilyDao {
    private final FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    @Override
    public List<AbstractMap.SimpleEntry<Integer, Family>> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        return familyDao.deleteFamily(index);
    }

    @Override
    public boolean deleteFamily(Family family) {
        return familyDao.deleteFamily(family);
    }

    @Override
    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    @Override
    public void displayAllFamilies() {
        familyDao.displayAllFamilies();
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int num) {
        return familyDao.getFamiliesBiggerThan(num);
    }

    @Override
    public List<Family> getFamiliesLessThan(int num) {
        return familyDao.getFamiliesLessThan(num);
    }

    @Override
    public int countFamiliesWithMemberNumber(int num) {
        return familyDao.countFamiliesWithMemberNumber(num);
    }

    @Override
    public void createNewFamily(Human father, Human mother) {
        familyDao.createNewFamily(father, mother);
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        return familyDao.adoptChild(family, child);
    }

    @Override
    public void deleteAllChildrenOlderThen(int age) {
        familyDao.deleteAllChildrenOlderThen(age);
    }

    @Override
    public int count() {
        return familyDao.count();
    }

    @Override
    public Set<Pet> getPets(int index) {
        return familyDao.getPets(index);
    }

    @Override
    public void addPet(int index, Pet... pet) {
        familyDao.addPet(index, pet);
    }
}