package hw_10;

public class Dog extends Pet implements Foul {

    @Override
    void respond() {
        System.out.println("Hello, host. I am " + super.getNickname() + ". I missed!");
    }

    public Dog() {
        super();
    }

    public Dog(String name) {
        super(name);
        setSpecies(Species.DOG);
    }

    public Dog(String name, int num, int trick, String... habits) {
        super(name, num, trick, habits);
        setSpecies(Species.DOG);
    }
}
