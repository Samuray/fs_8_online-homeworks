package hw_10;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Family {
   private Human mother;
   private Human father;
   private ArrayList<Human> children;
   private Set<Pet> myPet;

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public ArrayList<Human> getChildren() {
        return this.children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public Set<Pet> getMyPet() {
        return myPet;
    }

    public void setMyPet(Pet... values) {
        this.myPet = new HashSet<>();
        for (Pet value: values) {
            this.myPet.add(value);
        }
    }

    public ArrayList<Human> addChild(Human child) {
        ArrayList<Human> newChild = new ArrayList<>(children);
        newChild.add(child);
        child.setFamily(this);
        return this.children = newChild;
    }

    public boolean deleteChild(int num) {
        if (num < 0 || num >= this.children.size()) {
            System.out.println("Некорректный индекс");
            return false;
        }
        this.children.remove(num);
        return true;
    }


    public int countFamily() {
        int count = 2;
        for (int i = 0; i < this.children.size(); i++) {
            count++;
        }
        return count;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Family {\n");
        sb.append("  Mother: ").append(mother.getName()).append("\n");
        sb.append("  Father: ").append(father.getName()).append("\n");
        sb.append("  Children: ").append(this.children).append("\n");
        sb.append("  Pet: ").append(myPet.toString()).append("\n");
        sb.append("}");
        return sb.toString();
    }

    @Override
    protected void finalize() {
        System.out.println("Семья уходит в небытие...");
    }

    public Family(Human mam, Human dad, Pet... myPet) {
        if (mam != null && dad != null) {
            this.mother = mam;
            this.mother.setFamily(this);
            this.father = dad;
            this.father.setFamily(this);
            setMyPet(myPet);
            this.children = new ArrayList<Human>();
        } else {
            System.out.println("Error: Both mother and father must be present to create a family.");
        }
    }
}
