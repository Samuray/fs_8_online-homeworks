package hw_10;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class Main {
    public static long getTimeWithInt(int year, int month, int day){
        LocalDate birthDate = LocalDate.of(year, month, day);
        LocalDate now = LocalDate.now();
        Period period = Period.between(birthDate, now);
        long ageDays = period.getYears() * 365L + period.getMonths() * 30L + period.getDays();
        return ageDays * 24 * 60 * 60 * 1000;
    }

    public static long getTimeWithString(String date) {
        LocalDate newDate = LocalDate.parse(date, java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        long milliseconds = newDate.atStartOfDay().toEpochSecond(ZoneOffset.UTC) * 1000;
        return milliseconds;
    }

    public static void main(String[] args) {


        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        Human mother = new Human("Jane", "Karleone", getTimeWithInt(1945, 5, 20));
        Human father = new Human("Vito", "Karleone", getTimeWithInt(1948, 4, 15));
        Human sun = new Human("Bob", "Karleone", getTimeWithString("03/10/1985"));
        Human daughter = new Human("Mary", "Karleone", getTimeWithString("27/02/1990"));
        DomesticCat myCat = new DomesticCat( "Murzik", 5, 60, "eat", "drink", "sleep", "meows" );
        Dog myDog = new Dog( "Sharik", 3, 50,  "jump","gives paw");
        RoboCat robo = new RoboCat( "Zhorik", 2, 90, "eat", "floats", "walks on its hind legs");
        Fish myFish = new Fish( "Jaw",2, 99, "eat");
        Family myFamily = new Family(mother, father, robo, myCat);
        Family myFamily_2 = new Family(mother,father,myDog, robo);

        Man chel = new Man("Vasya", "Vasilek", getTimeWithInt(1990, 3,20));
        Woman chel2 = new Woman("Motya", "Tyotya", getTimeWithInt(1991, 5, 1));

        System.out.println("\nCreating a new family...");
        familyController.createNewFamily(new Human("JOHN","jhonson", getTimeWithInt(2000, 4, 12)), new Human("JANE","janson", getTimeWithInt(2000, 4, 12)));
        familyController.createNewFamily(new Human("BOB","Bobson", getTimeWithInt(2000, 4, 12)), new Human("BABY","Bobson", getTimeWithInt(2000, 4, 12)));

        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        familyController.adoptChild(myFamily, daughter);
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");


        System.out.println("\nCount of families: " + familyController.count());

        System.out.println("\nDeleting family at index " + 0 + "...");
        familyController.deleteFamily(0);

        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        familyController.createNewFamily(chel, chel2);

        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        familyController.saveFamily(myFamily_2);
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        System.out.println("\nDeleting family at object...");
        familyController.deleteFamily(myFamily_2);
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        familyController.saveFamily(myFamily_2);
        familyController.adoptChild(myFamily_2, sun);
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        int num = 3;
        System.out.println("Families with more " + num +  " people");
        System.out.println(familyController.countFamiliesWithMemberNumber(num));
        System.out.println("================================================================================================");

        int col = 2;
        System.out.println("Families Bigger " + col +  " people");
        System.out.println(familyController.getFamiliesBiggerThan(col));
        System.out.println("================================================================================================");
        col = 3;
        System.out.println("Families Less " + num +  " people");
        System.out.println(familyController.getFamiliesLessThan(col));
        System.out.println("================================================================================================");

        familyController.deleteAllChildrenOlderThen(35);
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        System.out.println(familyController.getPets(1));
        System.out.println("================================================================================================");

        familyController.addPet(1, myCat, myFish);
        System.out.println("\nAll families:");
        familyController.displayAllFamilies();
        System.out.println("================================================================================================");

        System.out.println(familyController.getFamilyByIndex(0));
        System.out.println("================================================================================================");

        System.out.println(familyController.getAllFamilies());


        System.out.println("=======================================================================================================================================================");

        System.out.println(daughter.describeAge());
    }
}
