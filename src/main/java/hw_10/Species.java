package hw_10;

public enum Species {
    CAT, DOG, HAMSTER, PARROT, FISH, ROBOCAT, DOMESTICCAT, UNKNOWN;
}
