package hw_11;

import java.util.AbstractMap;
import java.util.List;
import java.util.Set;

public class FamilyController {
    private static final int MAX_FAMILY_SIZE = 4;
    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<AbstractMap.SimpleEntry<Integer, Family>> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }

    public boolean deleteFamily(int index) {
        return familyService.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        return familyService.deleteFamily(family);
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int num) {
        return familyService.getFamiliesBiggerThan(num);
    }

    public List<Family> getFamiliesLessThan(int num) {
        return familyService.getFamiliesLessThan(num);
    }

    public int countFamiliesWithMemberNumber(int num) {
        return familyService.countFamiliesWithMemberNumber(num);
    }

    public void createNewFamily(Human father, Human mother) {
        familyService.createNewFamily(father, mother);
    }

    public Family bornChild (Family family, String manName, String womenName) {
        if (family.countFamily() >= MAX_FAMILY_SIZE) {
            throw new FamilyOverflowException("Семья не может принять больше членов");
        }
        return familyService.bornChild(family, manName, womenName);
    }

    public Family adoptChild(Family family, Human child) {
        if (family.countFamily() >= MAX_FAMILY_SIZE) {
            throw new FamilyOverflowException("Семья не может принять больше членов");
        }
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet... pet) {
        familyService.addPet(index, pet);
    }
}
