package hw_11;

public class FamilyOverflowException  extends RuntimeException{
    public FamilyOverflowException(String message) {
        super(message);
    }
}
