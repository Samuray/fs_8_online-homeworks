package hw_11;

public enum Species {
    CAT, DOG, HAMSTER, PARROT, FISH, ROBOCAT, DOMESTICCAT, UNKNOWN;
}
