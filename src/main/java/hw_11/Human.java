package hw_11;

import java.time.*;
import java.util.Iterator;
import java.util.Map;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;
    private Map<String, String> schedule;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public long getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }
    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }
    public int getIq() {
        return iq;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public Map<String, String> getSchedule() {
        return schedule;
    }
    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }


    public void greetPet() {
        Iterator <Pet> iterator = family.getMyPet().iterator();
        while (iterator.hasNext())
            System.out.println("Hello, " + iterator.next().getNickname() + "!");
    }
    public void describePet() {
        Iterator <Pet> iterator = family.getMyPet().iterator();
        while (iterator.hasNext()) {
            String trick;
            Pet currentPet = iterator.next();
            if( currentPet .getTrickLevel() > 50) {
                trick = "very cunning!";
                System.out.println("I have " + currentPet .getSpecies() + ", it is " + currentPet .getAge() + " years old, it is " + trick);
            } else {
                trick = "almost not cunning!";
                System.out.println("I have " + currentPet .getSpecies() + ", it is " + currentPet .getAge() + " years old, it is " + trick);
            }
        }
    }

    public String describeAge() {
        LocalDate currentDate = LocalDate.now();
        LocalDateTime birthDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(this.birthDate), ZoneOffset.UTC);
        Period period = Period.between(birthDateTime.toLocalDate(), currentDate);
        return  period.getYears() + " years, " + period.getMonths() + " months и " + period.getDays() + " days";
    }

    public String prettyFormat() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        sb.append("Name: ").append(getName()).append(" ");
        sb.append("Surname: ").append(getSurname()).append(", ");
        sb.append("Age: ").append(describeAge()).append(", ");
        if(iq != 0) {
            sb.append("Iq: ").append(getIq()).append(", ");
        }
        if(schedule != null && !schedule.isEmpty()) {
            sb.append("Schedule: ").append(this.schedule).append(", ");
        }
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public String toString() {
        return prettyFormat();
    }

    @Override
    protected void finalize() {
        System.out.println("Человек уходит в небытие...");
    }

    public Human() {}
    public Human(String firstName, String surName, long num) {
        this.name = firstName;
        this.surname = surName;
        this.birthDate = num;
    }
    public Human(String firstName, String surName, long num, int humanIq) {
        this.name = firstName;
        this.surname = surName;
        this.birthDate = num;
        this.iq = humanIq;
    }
    public Human(String firstName, String surName, long num, Family family) {
        this.name = firstName;
        this.surname = surName;
        this.birthDate = num;
        this.family = family;
    }
    public Human(String firstName, String surName, long num, int humanIq, Family myFamily, Map<String, String> sched ) {
        this.name = firstName;
        this.surname = surName;
        this.birthDate = num;
        this.iq = humanIq;
        this.family = myFamily;
        this.schedule = sched;
    }
}
