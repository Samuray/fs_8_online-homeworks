package hw_6;


public class Main {
    public static void main(String[] args) {

        DomesticCat myCat = new DomesticCat("Murzik", 5, 60, new String[]{"eat", "drink", "sleep"});
        System.out.println(myCat.toString());
        Dog myDog = new Dog("Sharik", 3, 50, new String[]{"eat", "jump","gives paw"});
        RoboCat robo = new RoboCat("Zhorik", 2, 90, new String[]{"eat", "eat", "eat"});
        System.out.println(myDog.toString());
        myCat.eat();
        robo.respond();
        myCat.foul();
        Dog newDog = new Dog();
        System.out.println(newDog.toString());

        System.out.println("=============" + "\n" + myCat.equals(myDog) + "\n" + "=============");

        DayOfWeek monday = DayOfWeek.MONDAY;
        DayOfWeek friday = DayOfWeek.FRIDAY;

        Human mother = new Human("Jane", "Karleone",1950);
        Human father = new Human("Vito", "Karleone", 1948);
        Human sun = new Human("Bob", "Karleone", 1985);
        System.out.println(sun.toString());
        Human daughter = new Human("Mary", "Karleone", 1987);

        Human man = new Human("Michael", "Karleone", 1977,90, new Family(mother, father, myDog), new String[][]{
                {monday.name(), "something"},
                {friday.name(), "something2"}
        } );
        System.out.println(man.toString());
        man.greetPet();
        man.describePet();

        Man chel = new Man("Vasya", "Vasilek", 1990);
        chel.greetPet();
        System.out.println(chel.repairCar("BMW"));
        Woman chel2 = new Woman("Motya", "Tyotya", 1990);
        chel2.greetPet();
        System.out.println(chel2.makeup(false));

        Family myFamily = new Family(mother, father, myCat);
        myFamily.addChild(sun);
        myFamily.addChild(daughter);
        int familyCount = myFamily.countFamily();
        System.out.println("=========================" + "\n" + "В этой семье: " +  familyCount + " человека!" + "\n" + "=========================");
        System.out.println(myFamily.toString());
        myFamily.deleteChild(0);
        int newFamilyCount = myFamily.countFamily();
        System.out.println("=========================" + "\n" + "В этой семье: " +  newFamilyCount + " человека!" + "\n" + "=========================");
        System.out.println(myFamily.toString());



        Family myFamily_2 = new Family(mother,father,myDog);
        myFamily_2.addChild(sun);
        int familyCount_2 = myFamily_2.countFamily();
        System.out.println("=========================" + "\n" + "В этой семье: " +  familyCount_2 + " человека!" + "\n" + "=========================");
        System.out.println(myFamily_2.toString());


////       Раскоментировать для проверки загруженности памяти
//        for(int i = 0; i < 250000; i++) {
//            Human human = new Human();
//            human = null;
//        }

    }

}
