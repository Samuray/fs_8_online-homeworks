package hw_6;

public class DomesticCat extends Pet implements Foul {
    @Override
    void respond() {
        System.out.println("Hello, host. I am " + super.getNickname() + ". I missed!");
    }


    public DomesticCat() {
        super();
    }

    public DomesticCat(String name) {
        super(name);
        setSpecies(Species.DOMESTICCAT);
    }

    public DomesticCat(String name, int num, int trick, String[] hab) {
        super(name, num, trick, hab);
        setSpecies(Species.DOMESTICCAT);
    }
}