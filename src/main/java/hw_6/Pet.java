package hw_6;

import java.util.Arrays;

 public abstract class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }


    public void eat() {
        System.out.println("I'm eating!");
    }
    abstract void respond();

    @Override
    public boolean equals(Object obj) {
        if(obj == this) return true;
        if(obj == null) return false;
        if(!(obj instanceof Pet that)) return false;
        return this.species.equals(that.species)
                &&this.nickname.equals(that.nickname)
                && this.age == that.age
                && this.trickLevel == that.trickLevel
                && Arrays.equals(this.habits, that.habits);
    }

    @Override
    public String toString() {
        String infoPet = species +  " { " + nickname + ", " +  age + ", " +  trickLevel + ", " + Arrays.toString(habits) + " }";
        return infoPet;
    }

    @Override
    protected void finalize() {
        System.out.println("Питомец уходит в небытие...");
    }

    public Pet() {
        this.species = Species.UNKNOWN;
    }
    public Pet(String name) {
        this.nickname = name;
    }
    public Pet(String name, int num, int trick, String[] hab) {
        this.nickname = name;
        this.age = num;
        this.trickLevel = trick;
        this.habits = hab;
    }

}
