package hw_6;

public class Fish extends Pet {
    @Override
    void respond() {}

    public Fish() {
        super();
    }

    public Fish(String name) {
        super(name);
        setSpecies(Species.FISH);
    }

    public Fish(String name, int num, int trick, String[] hab) {
        super(name, num, trick, hab);
        setSpecies(Species.FISH);
    }
}
