package hw_6;

public class RoboCat extends Pet {

    @Override
    void respond() {
        System.out.println("Hello, host. I am " + super.getNickname() + ". I missed!");
    }
    public RoboCat() {
        super();
    }

    public RoboCat(String name) {
        super(name);
        setSpecies(Species.ROBOCAT);
    }

    public RoboCat(String name, int num, int trick, String[] hab) {
        super(name, num, trick, hab);
        setSpecies(Species.ROBOCAT);
    }
}
