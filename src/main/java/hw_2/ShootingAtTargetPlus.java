package hw_2;

import java.util.Random;
import java.util.Scanner;

public class ShootingAtTargetPlus {
    public static void main(String[] args) {
        System.out.println("All Set. Get ready to rumble!");

        char[][] gameField = createGameField();
        placeTarget(gameField);
        printGameField(gameField);

        Scanner scanner = new Scanner(System.in);

        int hits = 0;

        while (hits < 3) {
            System.out.print("Enter row (1-5): ");
            int row = inputValue(scanner);

            System.out.print("Enter column (1-5): ");
            int col = inputValue(scanner);

            if (row < 1 || row > 5 || col < 1 || col > 5) {
                System.out.println("Invalid input. Row and column must be between 1 and 5.");
                continue;
            }

            if (isHit(gameField, row, col)) {
                System.out.println("Hit!");
                gameField[row][col] = 'x';
                printGameField(gameField);
                hits++;
            } else if (gameField[row][col] == '*') {
                System.out.println("You've already shot here. Try again.");
            } else {
                System.out.println("Miss!");
                gameField[row][col] = '*';
                printGameField(gameField);
            }
        }

        System.out.println("You have won!");

        scanner.close();
    }

    private static char[][] createGameField() {
        char[][] gameField = new char[6][6];

        for (int i = 0; i < gameField.length; i++) {
            for (int j = 0; j < gameField[i].length; j++) {
                gameField[i][j] = '-';
            }
        }

        return gameField;
    }

    private static void placeTarget(char[][] gameField) {
        Random rand = new Random();
        int targetRow = rand.nextInt(3) + 1;
        int targetCol = rand.nextInt(3) + 1;

        boolean isHorizontal = rand.nextBoolean();

        if (isHorizontal) {
            for (int i = targetCol; i < targetCol + 3; i++) {
                gameField[targetRow][i] = '$';
            }
        } else {
            for (int i = targetRow; i < targetRow + 3; i++) {
                gameField[i][targetCol] = '$';
            }
        }
    }

    private static void printGameField(char[][] gameField) {
        System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
        for (int i = 1; i < gameField.length; i++) {
            System.out.print(i + " | ");
            for (int j = 1; j < gameField[i].length; j++) {
                System.out.print(gameField[i][j] + " | ");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static int inputValue(Scanner scanner) {
        while (!scanner.hasNextInt()) {
            System.out.println("Invalid input. Please enter a number.");
            scanner.next();
        }
        return scanner.nextInt();
    }

    private static boolean isHit(char[][] gameField, int row, int col) {
        return gameField[row][col] == '$';
    }
}