package hw_2;

import java.util.Random;
import java.util.Scanner;

public class ShootingAtTarget {

    public static void main(String[] args) {

        System.out.println("All Set. Get ready to rumble!");
        char[][] gameField = createGameField();
        int[] target = placeTarget(gameField);

        while (true) {
            printGameField(gameField);
            int row = checkInput("Enter row to shoot: ");
            int col = checkInput("Enter column to shoot: ");

            row--;
            col--;

            if (gameField[row][col] == '$') {
                System.out.println("You have won!");
                gameField[row][col] = 'X';
                printGameField(gameField);
                break;
            } else {
                System.out.println("Miss!");
                gameField[row][col] = '*';
            }
        }
    }

    public static char[][] createGameField() {
        char[][] board = new char[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                board[i][j] = '-';
            }
        }
        return board;
    }

    public static void printGameField(char[][] board) {
        System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
        for (int i = 0; i < 5; i++) {
            System.out.print(i + 1 + " | ");
            for (int j = 0; j < 5; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
        }
    }

    public static int[] placeTarget(char[][] board) {
        Random random = new Random();
        int targetRow = random.nextInt(5);
        int targetCol = random.nextInt(5);
        board[targetRow][targetCol] = '$';
        return new int[]{targetRow, targetCol};
    }

    public static int checkInput(String prompt) {
        Scanner scanner = new Scanner(System.in);
        int input;
        while (true) {
            System.out.print(prompt);
            if (scanner.hasNextInt()) {
                input = scanner.nextInt();
                if (input >= 1 && input <= 5) {
                    break;
                } else {
                    System.out.println("Please enter a number between 1 and 5.");
                }
            } else {
                System.out.println("Please enter a valid number.");
                scanner.next();
            }
        }
        return input;
    }

}

