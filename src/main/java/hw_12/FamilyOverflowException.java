package hw_12;

public class FamilyOverflowException  extends RuntimeException{
    public FamilyOverflowException(String message) {
        super(message);
    }
}
