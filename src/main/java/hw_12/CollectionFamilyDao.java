package hw_12;

import java.io.*;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static hw_11.Main.getTimeWithString;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families;
    Stream<Family> stream;


    public CollectionFamilyDao() {
        this.families = new ArrayList<>();
    }

    @Override
    public List<AbstractMap.SimpleEntry<Integer, Family>> getAllFamilies() {
        return IntStream.range(0, families.size())
                .mapToObj(index -> new AbstractMap.SimpleEntry<>(index + 1, families.get(index)))
                .collect(Collectors.toList());
    }

    @Override
    public void loadData(List<Family> newFamilies) throws IOException {
        File file = new File("BD.bin");

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(newFamilies);
        } catch (IOException e) {
            System.err.println("Ошибка при сохранении данных: " + e.getMessage());
            throw e;
        }
    }

    @Override
    public List<Family> getData() throws IOException {
        File file = new File("BD.bin");

        if (file.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                families = (List<Family>) ois.readObject();
            } catch (IOException | ClassNotFoundException e) {
                System.err.println("Ошибка при загрузке данных: " + e.getMessage());
                try {
                    throw e;
                } catch (ClassNotFoundException ex) {
                    throw new RuntimeException(ex);
                }
            }
        } else {
            families = new ArrayList<>();
        }

        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < families.size()) {
            return families.get(index);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < families.size()) {
            families.remove(index);
            try {
                loadData(families);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        families.remove(family);
        try {
             loadData(families);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    @Override
    public void saveFamily(Family family) {
        if (families.contains(family)) families.remove(family);
        families.add(family);
        try {
            loadData(families);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void displayAllFamilies() {
        families.stream().forEach(family -> System.out.println(family.prettyFormat()));
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int num) {
        return families.stream()
                .filter(family -> family.countFamily() > num)
                .collect(Collectors.toList());
    }

    @Override
    public List<Family> getFamiliesLessThan(int num) {
        return families.stream()
                .filter(family -> family.countFamily() < num)
                .collect(Collectors.toList());
    }

    @Override
    public int countFamiliesWithMemberNumber(int num) {
        return (int) families.stream()
                .filter(family -> family.countFamily() == num).count();
    }

    @Override
    public void createNewFamily(Human father, Human mother) {
        Family newFamily = new Family(father, mother);
        saveFamily(newFamily);
    }

    @Override
    public Family bornChild(Family family, String manName, String womenName) {
        if(manName != null && !manName.isEmpty()) {
            family.addChild(new Human(manName,family.getFather().getSurname(),getTimeWithString("13/04/2000")));
        } else {
            family.addChild(new Human(womenName,family.getFather().getSurname(),getTimeWithString("20/07/2010")));
        }
        saveFamily(family);
        return family;
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        saveFamily(family);
        return family;
    }

    @Override
    public void deleteAllChildrenOlderThen(int age) {
        families.forEach(family -> {
            List<Human> childrenToRemove = new ArrayList<>();
            family.getChildren().forEach(child -> {
                LocalDate currentDate = LocalDate.now();
                LocalDateTime birthDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(child.getBirthDate()), ZoneOffset.UTC);
                Period period = Period.between(birthDateTime.toLocalDate(), currentDate);
                if (period.getYears() > age) {
                    childrenToRemove.add(child);
                }
            });
            family.getChildren().removeAll(childrenToRemove);
        });
        try {
            loadData(families);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public int count() {
        return families.size();
    }

    @Override
    public Set<Pet> getPets(int index) {
        Set<Pet> newPet = new HashSet<>();
        if (index >= 0 && index < families.size()) {
            newPet =  families.get(index).getMyPet();
        }
        return newPet;
    }

    @Override
    public void addPet(int index, Pet... pet) {
        if (index >= 0 && index < families.size()) {
            families.get(index).setMyPet(pet);
            try {
                loadData(families);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

