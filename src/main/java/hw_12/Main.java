package hw_12;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Stream;

import static hw_12.Main.getFamilies;

public class Main {
    public static long getTimeWithInt(int year, int month, int day) {
        LocalDate birthday = LocalDate.of(year, month, day);
        long milliseconds = birthday.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
        return milliseconds;
    }

    public static long getTimeWithString(String date) {
        LocalDate newDate = LocalDate.parse(date, java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        long milliseconds = newDate.atStartOfDay().toEpochSecond(ZoneOffset.UTC) * 1000;
        return milliseconds;
    }

    public static void getFamilies(FamilyController familyController) {
        try {
            familyController.getData();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void biggerThan(FamilyController familyController) {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введіть число: ");
            if (!scan.hasNextInt()) {
                throw new InputMismatchException("Введено не число!");
            }
            int number = scan.nextInt();
            System.out.println(familyController.getFamiliesBiggerThan(number));
        } catch (InputMismatchException e) {
            System.out.println("Помилка: " + e.getMessage());
        }
    }

    public static void lessThan(FamilyController familyController) {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введіть число: ");
            if (!scan.hasNextInt()) {
                throw new InputMismatchException("Введено не число!");
            }
            int number = scan.nextInt();
            System.out.println(familyController.getFamiliesLessThan(number));
        } catch (InputMismatchException e) {
            System.out.println("Помилка: " + e.getMessage());
        }
    }

    public static void memberNumber(FamilyController familyController) {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введіть число: ");
            if (!scan.hasNextInt()) {
                throw new InputMismatchException("Введено не число!");
            }
            int number = scan.nextInt();
            System.out.println(familyController.countFamiliesWithMemberNumber(number));
        } catch (InputMismatchException e) {
            System.out.println("Помилка: " + e.getMessage());
        }
    }

    public static void createFamily(FamilyController familyController) {
        try{
            Scanner scan = new Scanner(System.in);
            System.out.println("Введіть ім'я матері: ");
            String motherName = scan.nextLine();
            System.out.println("Введіть прізвище матері: ");
            String motherSurName = scan.nextLine();
            System.out.println("Введіть рік народження матері: ");
            int mYear = scan.nextInt();
            System.out.println("Введіть місяць народження матері: ");
            int mMounth = scan.nextInt();
            System.out.println("Введіть день народження матері: ");
            int mDay = scan.nextInt();
            System.out.println("Введіть iq матері: ");
            int mIq = scan.nextInt();
            scan.nextLine();
            System.out.println("Введіть ім'я батька: ");
            String fatherName = scan.nextLine();
            System.out.println("Введіть прізвище батька: ");
            String fatherSurName = scan.nextLine();
            System.out.println("Введіть рік народження батька: ");
            int fYear = scan.nextInt();
            System.out.println("Введіть місяць народження батька: ");
            int fMounth = scan.nextInt();
            System.out.println("Введіть день народження батька: ");
            int fDay = scan.nextInt();
            System.out.println("Введіть iq батька: ");
            int fIq = scan.nextInt();
            Human mother = new Human(motherName, motherSurName, getTimeWithInt(mYear, mMounth, mDay), mIq);
            Human father = new Human(fatherName, fatherSurName, getTimeWithInt(fYear, fMounth, fDay), fIq);
            familyController.createNewFamily(mother,father);
        } catch (DateTimeParseException | InputMismatchException e) {
            System.out.println("Помилка введення даних: " + e.getMessage());
        }
    }

    public static void deleteGetByIndex(FamilyController familyController) {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введіть порядковий номер сім'ї (ID): ");
            if (!scan.hasNextInt()) {
                throw new InputMismatchException("Введено не число!");
            }
            int id = scan.nextInt();

            List<AbstractMap.SimpleEntry<Integer, Family>> allFamilies = familyController.getAllFamilies();

            Optional<AbstractMap.SimpleEntry<Integer, Family>> familyEntry = allFamilies.stream()
                    .filter(entry -> entry.getKey().equals(id))
                    .findFirst();

            if (familyEntry.isPresent()) {
                int index = familyEntry.get().getKey() - 1;
                familyController.deleteFamily(index);
                System.out.println("Сім'я була видалена.");
            } else {
                System.out.println("Сім'я с вказаним порядковим номером не знайдена.");
            }
        } catch (InputMismatchException e) {
            System.out.println("Помилка: " + e.getMessage());
        }

    }

    public static void bornNewChild(FamilyController familyController) {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введіть порядковий номер сім'ї (ID): ");
            int id = scan.nextInt();
            scan.nextLine();

            List<AbstractMap.SimpleEntry<Integer, Family>> allFamilies = familyController.getAllFamilies();

            Optional<AbstractMap.SimpleEntry<Integer, Family>> familyEntry = allFamilies.stream()
                    .filter(entry -> entry.getKey().equals(id))
                    .findFirst();

            String boyName= null;
            String girlName= null;
            System.out.println("Введіть ім'я хлопчика, або натисніть Enter: ");
            boyName = scan.nextLine();
            if (boyName.isEmpty()) {
                System.out.println("Введіть ім'я дівчини: ");
                girlName = scan.nextLine();
            }
            if (familyEntry.isPresent()) {
                int index = familyEntry.get().getKey() - 1;
                Family correctFamily = familyController.getFamilyByIndex(index);
                correctFamily = familyController.bornChild(correctFamily, boyName, girlName);
                familyController.saveFamily(correctFamily);
            } else {
                System.out.println("Сім'я с вказаним порядковим номером не знайдена.");
            }
        } catch (FamilyOverflowException | InputMismatchException e) {
            System.out.println("Помилка: " + e.getMessage());
        }
    }

    public static void adoptNewChild(FamilyController familyController) {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введіть порядковий номер сім'ї (ID): ");
            int id = scan.nextInt();
            scan.nextLine();

            List<AbstractMap.SimpleEntry<Integer, Family>> allFamilies = familyController.getAllFamilies();

            Optional<AbstractMap.SimpleEntry<Integer, Family>> familyEntry = allFamilies.stream()
                    .filter(entry -> entry.getKey().equals(id))
                    .findFirst();

            if (familyEntry.isPresent()) {
                int index = familyEntry.get().getKey() - 1;
                Family correctFamily = familyController.getFamilyByIndex(index);

                System.out.println("Введіть ім'я: ");
                String name = scan.nextLine();
                System.out.println("Введіть прізвище: ");
                String surName = scan.nextLine();
                System.out.println("Введіть дату народження (в форматі dd/mm/yyyy: ");
                String data = scan.nextLine();
                System.out.println("Введіть рівень iq: ");
                int iq = scan.nextInt();

                Human child = new Human(name, surName, getTimeWithString(data), iq);
                familyController.adoptChild(correctFamily, child);
            } else {
                System.out.println("Сім'я с вказаним порядковим номером не знайдена.");
            }
        } catch (FamilyOverflowException | DateTimeParseException | InputMismatchException e) {
            System.out.println("Помилка: " + e.getMessage());
        }

    }

    public static void editFamily(FamilyController familyController) {
        Stream <String> inMenu = Stream.of("1. Народити дитину", "2. Усиновити дитину","3. Повернутися до головного меню");
        inMenu.forEach(System.out::println);
        Scanner scan = new Scanner(System.in);
        System.out.println("Виберіть один з пунктів: ");
        String number = scan.next().trim().toLowerCase();
        switch (number) {
            case "1":
                bornNewChild(familyController);
                break;
            case "2":
                adoptNewChild(familyController);
                break;
            case "3":
                break;
            default:
                System.out.println("Будь-ласка введіть корректне число!");
        }
    }

    public static void deleteOlderThen(FamilyController familyController) {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введіть вік старше якого будуть видалені діти: ");
            if (!scan.hasNextInt()) {
                throw new InputMismatchException("Введено не число!");
            }
            int age = scan.nextInt();
            familyController.deleteAllChildrenOlderThen(age);
        } catch (InputMismatchException e) {
            System.out.println("Помилка: " + e.getMessage());
        }


    }

    public static void main(String[] args) {
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);
        Scanner scan = new Scanner(System.in);
        boolean isTrue = true;
        System.out.println(getTimeWithString("03/10/1985"));

        Stream<String> menu = Stream.of(
                "- 1. Завантажити сім'ї з бази данних",
                "- 2. Відобразити весь список сімей (відображає список усіх сімей з індексацією, що починається з 1)",
                "- 3. Відобразити список сімей, де кількість людей більша за задану: запитати цікаве число",
                "- 4. Відобразити список сімей, де кількість людей менша за задану: запитати цікаве число",
                "- 5. Підрахувати кількість сімей, де кількість членів дорівнює: запитати цікаве число",
                "- 6. Створити нову родину:\n - запитати ім'я матері\n - запитати прізвище матері\n - Запитати рік народження матері\n - Запитати місяць народження матері\n - Запросити день народження матері\n - запитати iq матері\n - запитати ім'я батька\n - Запитати прізвище батька\n - Запитати рік народження батька\n - Запитати місяць народження батька\n - Запросити день народження батька\n - запитати батька iq",
                "- 7. Видалити сім'ю за індексом сім'ї у загальному списку: Запитати порядковий номер сім'ї (ID)",
                "- 8. Редагувати сім'ю за індексом сім'ї у загальному списку\n- 1. Народити дитину\n - Запитати порядковий номер сім'ї (ID)\n - Запитати необхідні дані (яке ім'я дати хлопчику, яке дівчинці)\n- 2. Усиновити дитину\n - Запитати порядковий номер сім'ї (ID)\n - запитати необхідні дані (ПІБ, рік народження, інтелект)\n- 3. Повернутися до головного меню",
                "- 9. Видалити всіх дітей старше віку (у всіх сім'ях видаляються діти старше зазначеного віку - вважатимемо, що вони виросли): Запитати цікавий вік)");
        menu.forEach(System.out::println);

        while (isTrue) {

            System.out.println("Зробіть ваш вибір: ");
            String number = scan.next().trim().toLowerCase();
            if (number.equals("exit")) {
                isTrue = false;
                System.out.println("Програма зачинена!");
            } else {
                switch (number) {
                    case "1":
                        getFamilies(familyController);
                        System.out.println("Родини завантажені!");
                        break;
                    case "2":
                        System.out.println(familyController.getAllFamilies());
                        break;
                    case "3":
                        System.out.println("Список сімей, де кількість людей більша за задану: ");
                        biggerThan(familyController);
                        break;
                    case "4":
                        System.out.println("Список сімей, де кількість людей менша за задану: ");
                        lessThan(familyController);
                        break;
                    case "5":
                        System.out.println("Кількість сімей, де кількість членів дорівнює: ");
                        memberNumber(familyController);
                        break;
                    case "6":
                        createFamily(familyController);
                        System.out.println("Родина створена!");
                        break;
                    case "7":
                        deleteGetByIndex(familyController);
                        break;
                    case"8":
                        editFamily(familyController);
                        break;
                    case "9":
                        deleteOlderThen(familyController);
                        break;
                    default:
                        System.out.println("Будь-ласка введіть корректне значення!");
                        break;
                }
            }

        }
    }
}
