package hw_12;

public class DomesticCat extends Pet implements Foul {
    @Override
    void respond() {
        System.out.println("Hello, host. I am " + super.getNickname() + ". I missed!");
    }


    public DomesticCat() {
        super();
    }

    public DomesticCat(String name) {
        super(name);
        setSpecies(Species.DOMESTICCAT);
    }

    public DomesticCat(String name, int num, int trick, String... habits) {
        super(name, num, trick, habits);
        setSpecies(Species.DOMESTICCAT);
    }
}