package hw_12;

import java.util.Map;

public final class Man extends Human {

    public String repairCar(String car){
        if (car != null && !car.isEmpty()) return car;
        else return "Nothing to fix!";
    }
    @Override
    public void greetPet() {
        System.out.println("Hello, I'm a man!");
    }

    public Man() {
    }

    public Man(String firstName, String surName, long num) {
        super(firstName, surName, num);
    }

    public Man(String firstName, String surName, long num, Family family) {
        super(firstName, surName, num, family);
    }

    public Man(String firstName, String surName, long num, int humanIq, Family myFamily, Map<String, String> sched) {
        super(firstName, surName, num, humanIq, myFamily, sched);
    }
}
