package hw_12;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Family implements Serializable {
   private Human father;
   private Human mother;
   private ArrayList<Human> children;
   private Set<Pet> myPet;

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public ArrayList<Human> getChildren() {
        return this.children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public Set<Pet> getMyPet() {
        return myPet;
    }

    public void setMyPet(Pet... values) {
        this.myPet = new HashSet<>();
        for (Pet value: values) {
            this.myPet.add(value);
        }
    }

    public ArrayList<Human> addChild(Human child) {
        ArrayList<Human> newChild = new ArrayList<>(children);
        newChild.add(child);
        child.setFamily(this);
        return this.children = newChild;
    }

    public boolean deleteChild(int num) {
        if (num < 0 || num >= this.children.size()) {
            System.out.println("Некорректный индекс");
            return false;
        }
        this.children.remove(num);
        return true;
    }


    public int countFamily() {
        int count = 2;
        for (int i = 0; i < this.children.size(); i++) {
            count++;
        }
        return count;
    }

    @Override
    public String toString() {
       return prettyFormat();
    }

    public String prettyFormat() {
        List<String> childs = this.children.stream().map(Human::prettyFormat).toList();
        String joinedChilds = String.join("\n          ", childs);
        Set<String> pets =  myPet.stream().map(Pet::prettyFormat).collect(Collectors.toSet());

        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("Family {\n");
        sb.append(" Mother: ").append(this.mother.prettyFormat()).append("\n");
        sb.append(" Father: ").append(this.father.prettyFormat()).append("\n");
        if(!joinedChilds.isEmpty()) {
            sb.append("  Children:\n          ").append(joinedChilds).append("\n");
        }
        if(!pets.isEmpty()) {
            sb.append("  Pet: ").append(pets).append("}");
        }
        sb.append("\n");
        return sb.toString();
    }

    @Override
    protected void finalize() {
        System.out.println("Семья уходит в небытие...");
    }

    public Family(Human mam, Human dad, Pet... myPet) {
        if (mam != null && dad != null) {
            this.mother = mam;
            this.mother.setFamily(this);
            this.father = dad;
            this.father.setFamily(this);
            setMyPet(myPet);
            this.children = new ArrayList<Human>();
        } else {
            System.out.println("Error: Both mother and father must be present to create a family.");
        }
    }
}
