package hw_12;

public interface Foul {
    default void foul() {
        System.out.println("You need to cover your tracks well...");
    }
}
